<?php
/**
 * Created by PhpStorm.
 * User: luthonda
 * Date: 17-08-2017
 * Time: 14:05
 */

class dbconection
{
    /**
     * @var string
     */
    private $hostname = 'localhost';
    /**
     * @var string
     */
    private $username = 'root';
    /**
     * @var string
     */
    private $password = '';

    /**
     * dbconection constructor.
     * @param string $hostname
     * @param string $username
     * @param string $password
     */
    public function __construct($hostname, $username, $password)
    {
        $this->hostname = $hostname;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return PDO
     */
    public function conection()
    {

        try {
            $dbh = new PDO("mysql:host=$this->hostname;dbname=stickercollections", $this->username, $this->password);

            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // <== add this line
            echo 'Connected to Database<br/>';

            return $dbh;
        } catch
        (PDOException $e) {
            echo $e->getMessage();
        }

    }

}