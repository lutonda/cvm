<?php
/**
 * Created by PhpStorm.
 * User: luthonda
 * Date: 17-08-2017
 * Time: 13:55
 */

interface iTempate{

    public function __construct();
    /**
     * @param int $id
     * @return mixed
     */
    public function mostrarUm(int $id);

    /**
     * @return mixed
     */
    public function mostrarTodos();

    /**
     * @param $obj
     * @return mixed
     */
    public function editar($obj);

    /**
     * @param int $id
     * @return mixed
     */
    public function eliminar(int $id);

    /**
     * @param Object $e
     * @return mixed
     */
    public function criar(Object $e);
}