<?php
/**
 * Created by PhpStorm.
 * User: luthonda
 * Date: 17-08-2017
 * Time: 13:41
 */

class Loja
{
    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $nome;
    /**
     * @var
     */
    private $endereco;
    /**
     * @var
     */
    private $contactos;

    /**
     * @return mixed
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * @param mixed $contactos
     */
    public function setContactos($contactos)
    {
        $this->contactos = $contactos;
    }
    /**
     * @var
     */
    private $estado;
    /**
     * @var
     */
    private $data;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}