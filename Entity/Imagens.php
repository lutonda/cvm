<?php
/**
 * Created by PhpStorm.
 * User: luthonda
 * Date: 17-08-2017
 * Time: 13:47
 */

class Imagens
{
    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $nome;
    /**
     * @var
     */
    private $descicao;
    /**
     * @var
     */
    private $album;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescicao()
    {
        return $this->descicao;
    }

    /**
     * @param mixed $descicao
     */
    public function setDescicao($descicao)
    {
        $this->descicao = $descicao;
    }

    /**
     * @return mixed
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * @param mixed $album
     */
    public function setAlbum($album)
    {
        $this->album = $album;
    }


}