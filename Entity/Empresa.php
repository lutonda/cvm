<?php
/**
 * Created by PhpStorm.
 * User: luthonda
 * Date: 17-08-2017
 * Time: 13:51
 */

class Empresa
{
    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $nome;
    /**
     * @var
     */
    private $descricao;
    /**
     * @var
     */
    private $sobre;
    /**
     * @var
     */
    private $contactos;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getSobre()
    {
        return $this->sobre;
    }

    /**
     * @param mixed $sobre
     */
    public function setSobre($sobre)
    {
        $this->sobre = $sobre;
    }

    /**
     * @return mixed
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * @param mixed $contactos
     */
    public function setContactos($contactos)
    {
        $this->contactos = $contactos;
    }
}